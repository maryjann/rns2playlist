# Copyright (c) 2020 Jakub Nowacki (maryjann@gmail.com)

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
import sys
import datetime
import time
import argparse
import spotipy
import spotipy.util as util
import re
import requests

scope = 'user-library-read, playlist-modify-private, playlist-read-private'

class ListEntry:
    year = 1970
    month = 1
    day  = 1
    hour = -1
    minute = -1
    artist = ''
    title = ''

    def to_seconds(self):
        return time.mktime((self.year, self.month, self.day, self.hour, self.minute, 0, 0, 0, -1))

class CmdArgs:
    username = ''
    client_id = ''
    client_secret = ''
    palylist = ''
    start_time = 0
    end_time = 0
    def __init__(self): 
        ap = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)
        ap.add_argument("-u", "--username", required=True,
                        help="Spotify username\n")
        ap.add_argument("-c", "--client_id", required=True,
                        help="Spotify Client ID\n" +
                             "Client ID is the unique identifier of your application.\n" +
                             "https://developer.spotify.com/documentation/general/guides/app-settings\n")
        ap.add_argument("-C", "--client_secret", required=True,
                        help="Spotify Client Secret\n" +
                             "Client Secret is the key that you pass in secure calls to the Spotify Accounts and Web API services.\n" +
                             "https://developer.spotify.com/documentation/general/guides/app-settings\n")
        ap.add_argument("-s", "--start", required=True,
                         help="Start time\n" +
                              "Format: YYYY-MM-DD HH:MM")
        ap.add_argument("-e", "--end", required=True,
                         help="End time\n" +
                              "Format: YYYY-MM-DD HH:MM")
        ap.add_argument("-p", "--playlist", required=False,
                         help="Name of Spotify playlist\n")

        args = vars(ap.parse_args())

        self.username = args['username']
        self.client_id = args['client_id']
        self.client_secret = args['client_secret']
        self.start_time = time.mktime(time.strptime(args['start'], "%Y-%m-%d %H:%M"))
        self.end_time = time.mktime(time.strptime(args['end'], "%Y-%m-%d %H:%M"))
        self.playlist = args['playlist']

def get_list():
    r = requests.get('https://rns-playlist.surge.sh/index.txt')
    entries = []

    idx = 0
    current_date = None
    for line in r.text.splitlines():
        le = ListEntry()
        if len(line) != 0:
            date = line[0:10]
            if len(re.findall('[a-zA-Z]', date)) != 0:
                continue
            if date.find(' ') == -1:
                le.year = int(date[0:4])
                le.month = int(date[5:7])
                le.day = int(date[8:10])
                current_date = le
            elif idx != 0:
                le.year = current_date.year
                le.month = current_date.month
                le.day = current_date.day
            line = line[11:]
            
            le.hour = int(line[0:2])
            le.minute = int(line[3:5])
            line = line[6:]

            le.artist = line[0 : line.find(' - ')]
            le.title = line[line.find(' - ') + 3: ]

            if (le.artist != "Radio Nowy Swiat" and le.artist != "Radio Nowy Świat"):
                if  idx == 0 or (le.artist != entries[idx - 1].artist and le.title != entries[idx - 1].title):
                    entries.append(le)
                    idx += 1        

    return entries

args = CmdArgs()
list = get_list()

os.environ["SPOTIPY_CLIENT_ID"] = args.client_id
os.environ["SPOTIPY_CLIENT_SECRET"] = args.client_secret

token = util.prompt_for_user_token(args.username,
                                   scope,
                                   client_id=args.client_id,
                                   client_secret=args.client_secret,
                                   redirect_uri='http://localhost:8080')

if token == None:
    exit(-1)

sp = spotipy.Spotify(auth=token)

if args.playlist == None and len(args.playlist) == 0:
    args.playlist = "RNŚ"
    
playlists = sp.user_playlists(args.username)
playlist = {}
while playlists:
    for i, entry in enumerate(playlists['items']):
        if  entry['name'] == args.playlist:
            playlist = entry
            break    
    if playlists['next']:
        playlists = sp.next(playlists)
    else:
        playlists = None

if (len(playlist) == 0):
    playlist = sp.user_playlist_create(args.username, args.playlist, False, "")

notFound = []
for entry in list:
    if entry.to_seconds() <= args.end_time and entry.to_seconds() >= args.start_time:
        result = sp.search(q='artist:'+ entry.artist +' track:' + entry.title, type='track', limit=10)
        items = result['tracks']['items']

        newItem = []
        select = 0

        if len(items) == 0:
            notFound.append(entry)
        else:
            print(items[select]['name'] + ' ' + items[select]['uri'])
            newItem.append(items[select]['uri'])
            sp.user_playlist_add_tracks(args.username, playlist_id=playlist['uri'], tracks=newItem)
        
print("\nNot found: ")        
for idx in range(0, len(notFound)):
    print(notFound[idx].artist + " - " + notFound[idx].title)
