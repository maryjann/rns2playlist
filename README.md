# README #

rns2playlist creates Spotify playlist based on data taken from https://rns-playlist.surge.sh/index.txt.
It is command line tool developped in Python

### Dependencies ###
#### Spotipy - https://spotipy.readthedocs.io
#### Spotify - access to Spotify API is required. Please visit https://developer.spotify.com/documentation/general/guides/app-settings/ create a Client ID and Client Secret

### Usage ###
usage: rns2playlist.py [-h] -u USERNAME -c CLIENT_ID -C CLIENT_SECRET -s START -e END [-p PLAYLIST]

optional arguments:
  -h, --help            show this help message and exit
  
  -u USERNAME, --username USERNAME
  
                        Spotify username
                        
  -c CLIENT_ID, --client_id CLIENT_ID
  
                        Spotify Client ID
                        
                        Client ID is the unique identifier of your application.
                        
                        https://developer.spotify.com/documentation/general/guides/app-settings
                        
  -C CLIENT_SECRET, --client_secret CLIENT_SECRET
  
                        Spotify Client Secret
                        
                        Client Secret is the key that you pass in secure calls to the Spotify Accounts and Web API services.
                        
                        https://developer.spotify.com/documentation/general/guides/app-settings
                        
  -s START, --start START
  
                        Start time
                        
                        Format: YYYY-MM-DD HH:MM
                        
  -e END, --end END     End time
  
                        Format: YYYY-MM-DD HH:MM
                        
  -p PLAYLIST, --playlist PLAYLIST
  
### Example ###
* Below command will create TEST playlist and add tracks that was played between on 20.07.2020 between 18:30 and 18:36:

		python3 rns2playlist.py -c <client ID> -C <client Secret> -s "2020-07-20 18:30" -e "2020-07-20 18:36" -u <spotify username> -p TEST
